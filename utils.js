export const BookItem = ({ name, author, price }) => {
   return `
      <li">
         <h2>${name}</h2>
         <div>by ${author}</div>
         <div>Price: ${price} &#36;</div>
      </li>`
}

export const checkProps = (book, bookListNumber) => {
   const mandatoryProps = ['name', 'author', 'price']
   const checkedProps = Object.keys(book)
   const missingProps = mandatoryProps.filter(prop => !checkedProps.includes(prop)).join(', ')
   const minMissingPropsLength = checkedProps.length < 2

   throw new Error(
       `Book with list-number ${bookListNumber} don't have "${missingProps}" ${!minMissingPropsLength ? 'property' : 'properties'} in description`
   )
}
