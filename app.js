import { books } from './data.js'
import {BookItem, checkProps} from './utils.js'

const renderElement = books => {
  const root = document.querySelector('.root')
  const booksList = document.createElement('ul')

  books.forEach((book, index) => {
    const { name, author, price } = book

    try {
      name && author && price ? (booksList.innerHTML += BookItem(book)) : checkProps(book, index)
    } catch (error) {
      console.error(error.message)
    }
  })

  root.append(booksList)
}

renderElement(books)
